package com.ars.controller;

import com.ars.util.FileUtils;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class DataControllerTest {

    private HttpServletRequest request;

    private HttpServletResponse response;

    private DataController controller;

    @Before
    public void setup() {
        controller = new DataController();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
    }

    /**
     * test retrieving folder content
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testFolderContentService() throws ServletException, IOException {
        File tmpFile = File.createTempFile("test-file", ".tmp");
        PrintWriter writer = new PrintWriter(tmpFile);

        doReturn(writer).when(response).getWriter();
        doReturn("/.m2").when(request).getPathInfo();

        controller.service(request, response);
        writer.close();

        String actulaResult = FileUtils.readFile(tmpFile);
        assertNotNull("Result should not be null", actulaResult);
        assertTrue("Not expected result", actulaResult.contains("\"name\":\"repository\""));
    }

    /**
     * test file download
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testFileContentService() throws ServletException, IOException {
        ServletOutputStream out = mock(ServletOutputStream.class);

        doReturn(out).when(response).getOutputStream();
        doReturn("/.m2/repository/commons-io/commons-io/2.5/commons-io-2.5.jar").when(request).getPathInfo();

        controller.service(request, response);
        verify(out, times(1)).write(any(byte[].class));
    }

    /**
     * file not found
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testNotFoundService() throws ServletException, IOException {
        String folderPath = "/non-exist-folder";
        doReturn(folderPath).when(request).getPathInfo();

        controller.service(request, response);
        verify(response, times(1)).sendError(HttpServletResponse.SC_NOT_FOUND, folderPath);
    }


}
