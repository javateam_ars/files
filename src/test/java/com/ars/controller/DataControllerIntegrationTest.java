package com.ars.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DataControllerIntegrationTest {

    private HttpServletRequest request;

    private HttpServletResponse response;

    private DataController controller;

    private final String USER_AGENT = "Mozilla/5.0";

    @Before
    public void setup() {
        controller = new DataController();
//        request = mock(HttpServletRequest.class);
//        response = mock(HttpServletResponse.class);
    }

    @Test
    public void testFolderContentService() throws Exception {
        String response = getHttpResponse("http://localhost:8080/list");

        assertNotNull(response);
        assertTrue(response.contains("<body>"));
        assertTrue(response.contains("</body>"));
        assertTrue(response.indexOf("</body>") - response.indexOf("<body>") > 5);
    }

    @Test
    public void testContentsJson() throws Exception {
        String json = getHttpResponse("http://localhost:8080/data");

        assertNotNull(json);
        assertTrue(json.length() > 0);

        List<Map<String, Object>> jsonResponse = getJsonMap(json);

        assertNotNull(jsonResponse);
        assertTrue(jsonResponse.size() > 0);
    }

    @Test
    public void testFileDownload() throws Exception {
        String json = getHttpResponse("http://localhost:8080/data");

        assertNotNull(json);
        assertTrue(json.length() > 0);

        List<Map<String, Object>> jsonResponse = getJsonMap(json);
        String file = null;
        for (Map<String, Object> map : jsonResponse) {
            boolean isFile = Boolean.valueOf(String.valueOf(map.get("file")));
            if (isFile) {
                file = String.valueOf(map.get("name"));
                break;
            }
        }

        if (file != null) {
            String fileContent = getHttpResponse("http://localhost:8080/data/" + file);

            assertNotNull(fileContent);
            assertTrue(fileContent.length() > 0);
        }
    }

    private String getHttpResponse(String urlString) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private List<Map<String, Object>> getJsonMap(String jsonListInput) throws IOException {
        return new ObjectMapper().readValue(jsonListInput, List.class);
    }

}
