
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>
	<h3>Requested Data [<%=request.getAttribute("javax.servlet.error.message")%>] Not Found</h3>
	<div>
		Go to main page <a href="<%=request.getContextPath()%>/">link</a>
	</div>
</body>
</html>
