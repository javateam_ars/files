<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="explorer">
<head>
	<title>FileManager</title>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/angular/angular.js"></script>

	<script>
        var contextPath = '<%=request.getContextPath()%>';
		var fileType = '<%= request.getAttribute("type")%>';
		var pathInfo = '<%=request.getAttribute("path")%>';
		var upPath = '<%=request.getAttribute("upPath")%>';
		var backPath = '<%=request.getAttribute("backPath")%>';
	</script>

	<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>
	<link rel="stylesheet" href="<%= request.getContextPath()%>/css/main.css" type="text/css" media="screen"/>
</head>
<body>
<div ng-controller="FileManager" id="file-manager">
	<div class="nav">
		<span ng-if="upPath != ''">
			<a href="{{upPath}}">Up</a>
		</span>
		<span ng-if="backPath != ''">
			<a href="{{backPath}}">Back</a>
		</span>
	</div>
	<table id="file-table">
		<tr>
			<th>Name</th>
			<th>Modified Date</th>
			<th>Type</th>
		</tr>
		<tr ng-repeat="item in items" class="item-row">
			<td class="item-name">
				<div ng-if="item.file" ng-click="loadFile(item.name)">
					<img src="<%=request.getContextPath()%>/image/icon/file.png" />
					<label>{{item.name}}</label>
				</div>
				<div ng-if="!item.file" ng-click="loadFolderContent(item.name)">
					<img src="<%=request.getContextPath()%>/image/icon/folder.png" />
					<label>{{item.name}}</label>
				</div>
			</td>
			<td>
				{{item.lastModified | date:'dd/MM/yyyy hh:mm:ss a'}}
			</td>
			<td>
				<label ng-if="item.file">File</label>
				<label ng-if="!item.file">File Folder</label>
			</td>
		</tr>
	</table>
</div>
</body>
</html>
