var explorer = angular.module('explorer', []);
var controllers = {};
explorer.controller(controllers);

controllers.FileManager = function ($scope, $http) {
    $scope.items = [];
    $scope.upPath = upPath;
    $scope.backPath = backPath;

    $scope.loadPath = function () {
        var path = contextPath + '/data' + pathInfo;
        console.log("PATH", path);

        $http.post(path).then(function (response) {
            $scope.items = response.data;
        });
    };

    $scope.loadFile = function (file) {
        var path = contextPath + '/data' + pathInfo + '/'  + file;
        console.log("FILE PATH", path);

        window.open(path);
    };

    if (fileType === 'file') {
        $scope.loadFile('');
    } else {
        $scope.loadPath();
    }

    $scope.endsWith = function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

    $scope.getLocation = function () {
        var l = document.createElement("a");
        l.href = '';
        if (!$scope.endsWith(l.pathname, '/')) {
            return l.pathname + '/';
        }
        return l.pathname;
    };

    $scope.loadFolderContent = function (folderName) {
        document.location = $scope.getLocation() + folderName;
    };

};
