package com.ars.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


public class ListController extends FileController {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// relative path of requested data
		String path = request.getPathInfo();
		String filePath;
		if (path == null || path.trim().length() == 0) {
			path = "";
		}
		filePath = directory + path.trim();

		File file = new File(filePath);
		if (!file.exists()) {
			// if requested file does not exist
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getPathInfo());
		} else {
			String upPath = "";
			if (path != "") {
				String parentPath = getParentRelativePath(file);
				if (parentPath != null) {
					upPath = request.getServletPath() + parentPath;
				}
			}
			request.setAttribute("upPath", upPath);

			String backPath = request.getHeader("referer");
			if (backPath == null) {
				backPath = "";
			}
			request.setAttribute("backPath", backPath);

			request.setAttribute("path", path);
			request.setAttribute("type", file.isFile() ? "file" : "folder");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/list.jsp");
			dispatcher.forward(request, response);
		}
	}

	private String getParentRelativePath(File file) {
		String parentRelPath = file.getParentFile().getAbsolutePath().substring(directory.length());
		if (parentRelPath != "") {
			parentRelPath = parentRelPath.replace("\\", "/");
		}
		return parentRelPath;
	}
}
