package com.ars.controller;

import com.ars.model.*;
import com.ars.util.FileUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


public class DataController extends FileController {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// relative path of requested data
		String path = request.getPathInfo();
		if (path == null || path.trim().length() == 0) {
			path = directory;
		} else {
			path = directory + path.trim();
		}

		File file = new File(path);
		if (!file.exists()) {
			// if requested file does not exist
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getPathInfo());
		} else {
			if (file.isFile()) {
				// if requested data is a file then sends it to download
				response.setContentType("application/octet-stream");
				response.setHeader("Content-disposition","attachment; filename=" + file.getName());

				OutputStream out = response.getOutputStream();
				FileUtils.readToStream(file, out);
			} else {
				// if requested data is folder
				JSONArray object = new JSONArray();
				File[] files = file.listFiles();
				if (files != null) {
					for (File f : files) {
						FileData itemData = new FileData();
						itemData.setName(f.getName());
						itemData.setFile(f.isFile());
						itemData.setLastModified(f.lastModified());

						object.add(itemData);
					}

					response.setContentType("application/json");
					response.getWriter().print(object);
				}
			}
		}
	}
}
