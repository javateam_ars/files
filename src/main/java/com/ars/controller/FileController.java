package com.ars.controller;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.File;

public class FileController extends HttpServlet {

    // default value if 'directory-path' is not set
    protected String directory = System.getProperty("user.home");

    @Override
    public void init(ServletConfig config) throws ServletException {
        String providedDirectory = System.getenv("directory-path");
        if (providedDirectory != null
                && providedDirectory.trim().length() > 0
                && new File(providedDirectory).exists()) {

            directory = providedDirectory;
        }
    }
}